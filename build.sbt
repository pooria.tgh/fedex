import Dependencies._

ThisBuild / version := "0.1.1-SNAPSHOT"

ThisBuild / scalaVersion := "2.12.8"

lazy val root = (project in file("."))
  .settings(
    name := "fedex"
  )

libraryDependencies ++= Seq(
  // akka streams
  "com.typesafe.akka" %% "akka-stream" % Versions.akka,
  // akka http
  "com.typesafe.akka" %% "akka-http"            % Versions.akkaHttp,
  "com.typesafe.akka" %% "akka-http-spray-json" % Versions.akkaHttp,
  "com.typesafe.akka" %% "akka-http-testkit"    % Versions.akkaHttp,
  // testing
  "com.typesafe.akka" %% "akka-testkit" % Versions.akka,
  "org.scalatest"     %% "scalatest"    % Versions.scalaTest,
  // JWT
  "com.pauldijou" %% "jwt-spray-json" % Versions.jwt,
  //iso
  "com.vitorsvieira" %% "scala-iso" % Versions.iso
)

//Circe
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % Versions.circe)

mainClass in Compile := Some("Main")

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
dockerBaseImage := "openjdk:jre"
