
First thing first, I'm going to show you the charts:

### **Sequence Diagram**

```mermaid
sequenceDiagram
    Client -->> Server: Request
    Server->>AggregatorService: New ?
    AggregatorService-->>ShipmentService: New !
    AggregatorService-->>TrackService: New !
    AggregatorService-->>PriceService: New !
    
    ShipmentService-->>backend-services: Get request
    TrackService-->>backend-services: Get request
    PriceService-->>backend-services:  Get request
    
    backend-services-->>ShipmentService:  Json
    backend-services-->>TrackService:  Json
    backend-services-->>PriceService:  Json
    
    ShipmentService->>AggregatorService: Result
    TrackService->>AggregatorService: Result
    PriceService->>AggregatorService: Result
    AggregatorService->>Server: Aggregated Result
    Server -->> Client: Response

```

### **AggregatorService State Diagram**

```mermaid
stateDiagram-v2
    [*] --> Receive
    state if_stateDelete <<choice>>
    Receive --> New
    
    New --> Tell
    Tell -->ShipmentService
    Tell -->TrackService
    Tell -->PriceService   

    Receive --> Result
    Result --> Update
    
    Update --> if_stateDelete

    if_stateDelete -->Send: Result is ready 
    if_stateDelete --> Continue: Result is not ready
    Send --> Delete
```


### **ShipmentService State Diagram**

```mermaid
stateDiagram-v2
    [*] --> Receive
    Receive --> New
    
    New --> PushDown
    PushDown --> Stream   

    Receive --> Result
    Result --> Send
    
    
    state Stream {
        [*] --> groupedWithin
        groupedWithin --> merge
        merge --> HttpRequest
        HttpRequest --> Decode 
        Decode --> Seperate
        Seperate --> Result
    }
```

## Design decisions:

- I decided to use Akka-stream here specifically because if `groupwithin` function and buffering backpressure to finish AS-2, and AS-3!

- However, I needed to aggregate the data, so I kept them in memory on the AggregatorService.

- The in-memory approach was chosen because I believe we won't miss long-run operation results if the service goes down (Anyway, SLA=10).

- With multiple instances and load balancing, we can quickly scale up this service.

- Since the SLA is 5 sec for your Backend-service(Docker file), I decided to use 5 sec timeout for the HttpRequest response.

- Due to my time constraints and the importance of meeting SLAs, I chose to focus on Load Testing rather than Unit Testing.


## Running
   first, you need to run the following command where you have build.sbt:

    sbt docker:publishLocal

then, you can run the following command, to check you have the docker image:

``` 
    docker images
```

Next, If you saw the image: fedex 0.1.1-SNAPSHOT. Then:


``` 
    cd /deployment
```

Finally

    docker-compose up

Service is ready on port locaclhost:8050

- [Example link](http://localhost:8050/aggregation?pricing=NL&track=123568918&shipments=109347237)
