import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.testkit.{TestDuration, TestKit}
import models.api.{CountryCode, ShipmentId, TrackId}
import modules.Server
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import services.AggregatorService

import scala.concurrent.duration.DurationInt

class AggregationSpec
    extends WordSpecLike
    with ScalatestRouteTest
    with Matchers
    with BeforeAndAfterAll
    with AggregatorService.AggregationJsonSupport1 {

  val serviceAgg: ActorRef  = system.actorOf(Props(new AggregatorService), "AggService")
  val serverForTest: Server = Server(8010, "localhost", serviceAgg)

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  implicit val timeout: RouteTestTimeout = RouteTestTimeout(10.seconds.dilated)

  "An aggregation service" must {

    "return status Ok" in {

      val request = Get("/aggregation?shipments=123456789&track=123456789&pricing=NL")

      request ~> serverForTest.routes ~> check {
        status shouldEqual StatusCodes.OK
      }

    }

    "99 Percent SLA" in {

      val request = Get("/aggregation?shipments=123456789&track=123456789&pricing=NL")

      request ~> serverForTest.routes ~> check {
        status shouldEqual StatusCodes.OK
      }

      request ~> serverForTest.routes ~> check {
        status shouldEqual StatusCodes.OK
      }

      request ~> serverForTest.routes ~> check {
        status shouldEqual StatusCodes.OK
      }

      request ~> serverForTest.routes ~> check {
        status shouldEqual StatusCodes.OK
      }

      request ~> serverForTest.routes ~> check {
        status shouldEqual StatusCodes.OK
      }

    }

  }

  "model" must {
    "be invalid for ShipmentId" in {
      an[IllegalArgumentException] should be thrownBy ShipmentId("12345678")
    }
    "be valid for ShipmentId" in {
      ShipmentId("123456789") shouldBe a[ShipmentId]
    }

    "be invalid for trackId" in {
      an[IllegalArgumentException] should be thrownBy TrackId("12345678")
    }

    "be valid for trackId" in {
      TrackId("123456789") shouldBe a[TrackId]
    }

    "be invalid for Country Code" in {
      an[IllegalArgumentException] should be thrownBy CountryCode("DER")
    }

    "be valid for Country Code" in {
      CountryCode("NL") shouldBe a[CountryCode]
    }

  }

}
