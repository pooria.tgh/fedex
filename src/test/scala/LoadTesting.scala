import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.typesafe.config.ConfigFactory
import modules.Server
import services.AggregatorService

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Success

object LoadTesting extends App {

  implicit val system: ActorSystem = ActorSystem("load-test-system")
  val config                       = ConfigFactory.load()

  implicit val exec: ExecutionContextExecutor  = system.dispatcher
  implicit val materializer: ActorMaterializer = akka.stream.ActorMaterializer()

  val service = system.actorOf(Props(new AggregatorService()), s"AggService")

  val serverForTest: Server = Server(8010, "localhost", service)
  serverForTest.start()

  val urls = (1 to 1000)
    .map(_ % 10)
    .map(
      i =>
        s"http://localhost:8010/aggregation?pricing=NL,IR&track=55555555$i,55555544$i&shipments=12345678$i,12345672$i"
    )

  Source(urls)
    .throttle(500, 5.second)
    .map(url => HttpRequest(HttpMethods.GET, Uri(url)))
    .map(request => Http().singleRequest(request))
    .map(
      response =>
        response.map { res =>
          println(res)
          res.status.isSuccess
        }
    )
    .map(future => future.map(bool => if (bool) 1 else 0))
    .runFold(Future(0))((acc, future) => acc.flatMap(x => future.map(y => x + y)))
    .map(
      x =>
        x.onComplete {
            case Success(x) => println(x * 100.0 / urls.length)
            case _          => println("error")
          }
    )

}
