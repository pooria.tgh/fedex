package config

object Config {

  def pricingEndpoint = "http://localhost:8080/pricing"

  def shipmentEndpoint = "http://localhost:8080/shipments"

  def trackEndpoint = "http://localhost:8080/track"
}
