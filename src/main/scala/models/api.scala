package models

import com.vitorsvieira.iso._

object api {
  case class ShipmentId(value: String) {
    require(value.length == 9, "ShipmentId must  be length 9")
  }

  case class TrackId(value: String) {
    require(value.length == 9, "trackId must  be length 9")
  }

  case class CountryCode(value: String) {
    require(ISOCountry.from(value).isDefined, "Pricing must be a valid ISO country code")
  }

  class AggregationRequest(
      val shipments: Iterable[ShipmentId],
      val track: Iterable[TrackId],
      val pricing: Iterable[CountryCode]
  )

  object AggregationRequest {
    def apply(shipments: String, track: String, pricing: String): AggregationRequest = {
      new AggregationRequest(
        shipments.split(",").map(ShipmentId),
        track.split(",").map(TrackId),
        pricing.split(",").map(CountryCode)
      )
    }
  }

}
