import akka.actor.{ ActorSystem, Props }
import akka.stream.ActorMaterializer
import com.typesafe.config.{ Config, ConfigFactory }
import modules.Server
import services.AggregatorService

import scala.concurrent.ExecutionContextExecutor
import scala.language.postfixOps

object Main {

  implicit val system: ActorSystem             = ActorSystem("AkkaStreamsRecap")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor    = system.dispatcher
  val config: Config                           = ConfigFactory.load()

  def main(args: Array[String]): Unit = {

    val serviceAgg = system.actorOf(Props(new AggregatorService()), "AggService")
    Server(8050, "localhost", serviceAgg).start()

  }
}
