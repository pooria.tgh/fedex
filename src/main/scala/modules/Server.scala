package modules

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, HttpResponse, StatusCode }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import models.api.AggregationRequest
import modules.Server.timeout
import services.AggregatorService
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.{ Failure, Success }

class Server(port: Int, host: String, service: ActorRef)(
    implicit system: ActorSystem,
    ec: scala.concurrent.ExecutionContext,
    materializer: ActorMaterializer
) {

  val routes: Route =
    pathPrefix("healthCheck") {
      path("readiness") {
        complete("Ok")
      } ~
        path("aliveness") {
          complete("Ok")
        }
    } ~
      path("aggregation") {
        get {
          parameters("shipments".as[String], "track".as[String], "pricing".as[String])
            .as[AggregationRequest](AggregationRequest.apply _) { requestParams: AggregationRequest =>
              val res = service ? AggregatorService.New(
                requestParams.track.map(_.value).toList,
                requestParams.shipments.map(_.value).toList,
                requestParams.pricing.map(_.value).toList
              )

              onComplete(res) {
                case Success(result) =>
                  val response = HttpResponse(
                    entity = HttpEntity(ContentTypes.`application/json`, result.toString)
                  )
                  complete(response)
                case Failure(ex) =>
                  val response = HttpResponse(
                    entity = HttpEntity(ContentTypes.`application/json`, ex.getMessage),
                    status = StatusCode.int2StatusCode(503)
                  )
                  complete(response)
              }
            }
        }
      }

  val bindingFuture: Future[Http.ServerBinding] = akka.http.scaladsl.Http().bindAndHandle(routes, host, port)

  def start(): Unit = {
    bindingFuture.onComplete {
      case scala.util.Success(binding) ⇒
        println(s"Server bound to ${binding.localAddress}")
      case scala.util.Failure(e) ⇒
        println(s"Server could not bind to $host:$port: ${e.getMessage}")
    }
  }

}

object Server {

  implicit val timeout: Timeout = Timeout(20.seconds)

  def apply(
      port: Int,
      host: String,
      service: ActorRef
  )(implicit system: ActorSystem, ec: scala.concurrent.ExecutionContext, materializer: ActorMaterializer): Server = {
    new Server(port, host, service)
  }

}
