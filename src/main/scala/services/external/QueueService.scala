package services.external

import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.event.LoggingAdapter
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import services.external.QueueService.{Agg, actorSource}
import spray.json.DefaultJsonProtocol._

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.language.{higherKinds, postfixOps}

abstract class QueueService(val url: String)(
    implicit system: ActorSystem,
    materializer: ActorMaterializer,
    ec: ExecutionContext,
    log: LoggingAdapter
) extends Actor {

  var aggAtor: Option[ActorRef] = None

  val actorRef: ActorRef = actorSource
    .groupedWithin(5, 5 seconds)
    .to(Sink.foreach { p =>
      val responseFuture: Future[HttpResponse] = sendRequest(p)
      manageResponse(p, responseFuture)
    })
    .run()

  def handleSuccessResponse(p: Seq[Agg], response: HttpResponse): Unit

  def handleFailedRequest(p: Seq[Agg]): Unit

  def call(params: Map[String, String]): Future[HttpResponse] =
    Http()
      .singleRequest(
        HttpRequest(
          uri = Uri(url)
            .withQuery(Query(params))
        ),
        settings = ConnectionPoolSettings(system)
          .withConnectionSettings(ClientConnectionSettings(system).withConnectingTimeout(10 seconds))
          .withIdleTimeout(10 seconds)
      )

  private def manageResponse(p: Seq[Agg], responseFuture: Future[HttpResponse]): Future[Unit] = {
    responseFuture
      .map {
        case response @ HttpResponse(StatusCodes.OK, _, entity, _) =>
          handleSuccessResponse(p, response)

        case other =>
          handleFailedRequest(p)
          log.error(s"Undefined queue service response:$other")

      }
      .recover {
        case ex =>
          log.error(ex, "Queue service issue")
          handleFailedRequest(p)
      }
  }

  private def sendRequest(p: Seq[Agg]): Future[HttpResponse] = {
    val param                                = p.map(_.id).mkString(",")
    val params                               = Map("q" -> param)
    val responseFuture: Future[HttpResponse] = call(params)
    responseFuture
  }

  override def receive: Receive

}

object QueueService {

  sealed trait Commands

  sealed trait Event

  case class New(countries: List[String], clientSender: ActorRef) extends Commands

  case class Agg(id: String, sender: ActorRef)

  val actorSource: Source[Agg, ActorRef] =
    Source.actorRef[Agg](bufferSize = 10000, overflowStrategy = OverflowStrategy.dropHead)

}
