package services.external

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.LoggingAdapter
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import config.Config
import services.external.QueueService.Agg
import spray.json.DefaultJsonProtocol._

import scala.concurrent.ExecutionContext
import scala.language.{ higherKinds, postfixOps }
import scala.util.{ Failure, Success }

class ShipmentQueueImpl(
    implicit system: ActorSystem,
    materializer: ActorMaterializer,
    ec: ExecutionContext,
    log: LoggingAdapter
) extends QueueService(Config.shipmentEndpoint) {

  override def handleSuccessResponse(p: Seq[Agg], response: HttpResponse): Unit = {

    val r = Unmarshal(response).to[Map[String, List[String]]].map { response =>
      response
    }

    r.onComplete {
      case Success(value) =>
        p.foreach { d =>
          self ! ShipmentQueueImpl.Result(d.id, d.sender, value.get(d.id))
        }
      case Failure(exception) =>
        log.error(exception, "Error in QueueService")
        p.foreach { d =>
          self ! ShipmentQueueImpl.Result(d.id, d.sender, None)
        }

    }
    response.discardEntityBytes()
  }

  override def handleFailedRequest(p: Seq[Agg]): Unit = {
    p.foreach { d =>
      self ! ShipmentQueueImpl.Result(d.id, d.sender, None)
    }
  }

  override def receive: Receive = {
    case QueueService.New(ids, clientSender) =>
      ids.foreach { p =>
        actorRef ! Agg(p, clientSender)
      }
      aggAtor = Some(sender())

    case r @ ShipmentQueueImpl.Result(_, _, _) =>
      aggAtor.get ! r

  }
}

object ShipmentQueueImpl {
  case class Result(id: String, sender: ActorRef, price: Option[List[String]])
}
