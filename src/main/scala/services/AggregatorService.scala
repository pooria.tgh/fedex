package services

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.event.{ Logging, LoggingAdapter }
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import io.circe.generic.auto._
import io.circe.syntax.EncoderOps
import services.AggregatorService.{ AggregationJsonSupport1, RequestsModel, ResponseModel, StateModel }
import services.external.{ PriceQueueImpl, QueueService, ShipmentQueueImpl, TrackQueueImpl }
import spray.json.{ DefaultJsonProtocol, RootJsonFormat }

import scala.concurrent.ExecutionContext
import scala.language.postfixOps

class AggregatorService(
    implicit system: ActorSystem,
    implicit val materializer: ActorMaterializer,
    implicit val ec: ExecutionContext
) extends Actor
    with AggregationJsonSupport1 {

  implicit val log: LoggingAdapter = Logging(context.system, this)

  val shipmentService: ActorRef = system.actorOf(Props(new ShipmentQueueImpl))

  val trackService: ActorRef = system.actorOf(Props(new TrackQueueImpl))

  val pricingService: ActorRef = system.actorOf(Props(new PriceQueueImpl))

  var state = Map.empty[ActorRef, StateModel]

  override def receive: Receive = {

    case AggregatorService.New(tId, sId, c) =>
      startNewRequest(tId, sId, c)

    case r @ ShipmentQueueImpl.Result(_, sender, _) =>
      state.get(sender).foreach { old =>
        state = updateState(state, r, old)
        fireFinishedStatesEvent(state)
        state = removeFinishedStates(state)
      }

    case r @ TrackQueueImpl.Result(_, sender, _) =>
      state.get(sender).foreach { old =>
        state = updateState(state, r, old)
        fireFinishedStatesEvent(state)
        state = removeFinishedStates(state)
      }

    case r @ PriceQueueImpl.Result(_, sender, _) =>
      state.get(sender).foreach { old =>
        state = updateState(state, r, old)
        fireFinishedStatesEvent(state)
        state = removeFinishedStates(state)
      }

    case other =>
      log.error(s"Undefined message received by Aggregator service$other")
  }

  private def removeFinishedStates(currentState: Map[ActorRef, StateModel]): Map[ActorRef, StateModel] = {
    currentState.filter(_._2.isFinish == false)
  }

  private def fireFinishedStatesEvent(currentState: Map[ActorRef, StateModel]): Unit = {
    currentState.filter(_._2.isFinish).foreach { d =>
      d._1 ! finalResult(d)
    }
  }

  private def startNewRequest(tId: List[String], sId: List[String], c: List[String]): Unit = {
    state = state + (sender() -> StateModel(RequestsModel(sId, tId, c), ResponseModel()))
    shipmentService ! QueueService.New(sId, sender())
    trackService ! QueueService.New(tId, sender())
    pricingService ! QueueService.New(c, sender())
  }

  private def updateState[A](
      currentState: Map[ActorRef, StateModel],
      result: A,
      old: StateModel
  ): Map[ActorRef, StateModel] = {

    result match {
      case ShipmentQueueImpl.Result(id, sender, status) =>
        currentState + (sender -> old.copy(
          responses = old.responses.copy(shipments = old.responses.shipments + (id -> status))
        ))
      case TrackQueueImpl.Result(id, sender, status) =>
        currentState + (sender -> old.copy(
          responses = old.responses.copy(track = old.responses.track + (id -> status))
        ))
      case PriceQueueImpl.Result(id, sender, status) =>
        currentState + (sender -> old.copy(
          responses = old.responses
            .copy(pricing = old.responses.pricing + (id -> status))
        ))
      case other => throw new Exception(s"Undefined service result message received by Aggregator service {$other}")
    }
  }

  private def finalResult(d: (ActorRef, StateModel)): String = {
    d._2.responses.asJson.noSpaces
  }
}

object AggregatorService {

  case class RequestsModel(shipmentIds: List[String], trackIds: List[String], pricingIds: List[String])

  case class ResponseModel(
      pricing: Map[String, Option[BigDecimal]] = Map(),
      track: Map[String, Option[String]] = Map(),
      shipments: Map[String, Option[List[String]]] = Map()
  )

  case class StateModel(requests: RequestsModel, responses: ResponseModel) {

    def isFinish: Boolean = {
      this.requests.shipmentIds.distinct.length == this.responses.shipments.size &&
      this.requests.trackIds.distinct.length == this.responses.track.size &&
      this.requests.pricingIds.distinct.length == this.responses.pricing.size
    }

  }

  trait AggregationJsonSupport1 extends SprayJsonSupport with DefaultJsonProtocol {
    implicit val responseModel: RootJsonFormat[ResponseModel] = jsonFormat3(ResponseModel)
  }

  case class New(tracksId: List[String], shipmentsId: List[String], countries: List[String])
}
