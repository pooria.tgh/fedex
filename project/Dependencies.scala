object Dependencies {

  object Versions {

    val iso       = "0.1.2"
    val jwt       = "2.1.0"
    val akka      = "2.5.20"
    val akkaHttp  = "10.1.7"
    val scalaTest = "3.0.5"
    val circe     = "0.14.1"
  }

}
